DROP TABLE IF EXISTS award;
DROP TABLE IF EXISTS dog;

CREATE TABLE dog(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    breed VARCHAR(255),
    age INT
);

CREATE TABLE award(
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    id_dog INT NOT NULL,
    Foreign Key (id_dog) REFERENCES dog(id) ON DELETE CASCADE 
);

INSERT INTO dog (name,breed,age) VALUES ("Fido", "Corgi", 3),("Rex", "Dalmatian", 10),("Robert", "Corgi", 13);

INSERT INTO award (label, date, id_dog) VALUES ("Best Dog 2022", "2022-04-04", 1), ("Okay Dog 2023", "2023-03-01", 2), ("Prix de la meilleure baguette 2021", "2021-04-23", 2);