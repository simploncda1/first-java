package co.simplon.alt6;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.alt6.dao.DogDaoFromAbstract;
import co.simplon.alt6.dao.DogDaoImpl;
import co.simplon.alt6.dao.interfaces.Dao;
import co.simplon.alt6.dao.interfaces.DogDao;
import co.simplon.alt6.entity.Dog;
import co.simplon.alt6.oop.HTMLDocument;
import co.simplon.alt6.oop.Person;
import co.simplon.alt6.oop.Promo;
import co.simplon.alt6.oop.Task;
import co.simplon.alt6.oop.Teacher;

public class Main {


    public static void main(String[] args) {
        DogDao dao = new DogDaoFromAbstract();

        dao.getAll();
    }

    public static void reflectionExample(String[] args) {
        Class<Dog> type = Dog.class;
        Dog dog = new Dog(1, "test", "toust", 10);
        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
            String capitalized = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
            try {
                System.out.println(type.getMethod("get"+capitalized).invoke(dog));
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
                    | NoSuchMethodException | SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        

        Dao<Dog> dao = new DogDaoImpl();
        
        // Dog dog = new Dog("test", "test", 10);
        // dao.add(dog);

        // System.out.println(dog.getId());

    }

    public static void archiveOOP(String[] args) {

        archive();
        HTMLDocument doc = new HTMLDocument();

        doc.addElement(new Task("do Stuff"));
        doc.addElement(new Person("Nom", "Prénom", 34));

        System.out.println(doc.toHTML());

    }

    public static void archive() {

        // Pyramid pyramid = new Pyramid();
        // pyramid.draw(6, false);

        Person person1 = new Person("Demel", "Jon", 37);
        Person person2 = new Person("Colombel", "Audrey", 30);
        Person person3 = new Person("Demel", "Paul", 45);
        // person1.greeting();
        // person2.greeting();
        person1.salute(person2);
        person1.salute(person1);
        person1.salute(person3);

        Promo promoAlt6 = new Promo(20);

        for (int i = 0; i < 13; i++) {
            promoAlt6.addStudent(new Person("name " + i, "firstname " + i, i));
        }

        promoAlt6.addStudent(new Teacher("Prof", "Prof", 24));

        promoAlt6.seatPlan(4);

        Integer age = null;

        long test = 10000000000l;

        double prix = 3.4;

        boolean isOver = false;

        char lettre = 'u';

        String prenom = "Jean";

        // Les tableaux primitifs en Java n'ont presqu'aucune méthode propre
        // et doivent avoir une taille définie à leur création rendant leur
        // utilisation un peu fastidieuse dans certains contextes
        String[] messages = new String[6];
        messages[0] = "test";

        // Les List sont des objets complexe dont le comportement se rapproche
        // d'avantage des array JS par exemple, dont la taille n'est pas fixe
        // (en tout cas pour les ArrayList classique) mais qui ne peuvent contenir
        // que des types complexe (on ne peut pas faire un List<int>, mais List<Integer>
        // oui)
        List<String> listeMessage = new ArrayList<>();
        listeMessage.add("prenom");
        listeMessage.size();
        listeMessage.get(0);

    }
}