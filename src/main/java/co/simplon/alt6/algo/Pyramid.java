package co.simplon.alt6.algo;

public class Pyramid {

    public void draw(int nbFloor, boolean inverted) {

        int nbStar = 1;
        int nbSpace = nbFloor-1;
        int changeStar = 2;
        int changeSpace = -1;
        if (inverted) {
            changeStar = -2;
            changeSpace = 1;
            nbStar = nbFloor*2-1;
            nbSpace = 0;
        }

        String star = "*";
        String space = " ";
        for (int i = 0; i < nbFloor; i++) {
            System.out.print(space.repeat(nbSpace));
            System.out.println(star.repeat(nbStar));
            nbStar += changeStar;
            nbSpace += changeSpace;

        }

    }
}
