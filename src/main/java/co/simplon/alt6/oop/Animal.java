package co.simplon.alt6.oop;

public abstract class Animal {
    
    protected String name;
    

    public void eat() {
        breath();
        System.out.println("Ingest food");
    }

    public abstract void breath();

}
