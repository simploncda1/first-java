package co.simplon.alt6.oop;

import java.util.ArrayList;
import java.util.List;

public class ComplexTask extends Task {
    public List<Task> subTasks = new ArrayList<>();

    public ComplexTask(String label) {
        super(label);
    }

    public ComplexTask(String label, List<Task> subTasks) {
        super(label);
        this.subTasks = subTasks;
    }

    @Override
    public String toString() {
        String result = super.toString();
        for (Task task : this.subTasks) {
            result += "\n  " + task;
        }

        return result;

    }

    @Override
    public void toggle() {
        super.toggle();
        for (Task task : this.subTasks) {
            if (task.done != this.done) {
                task.toggle();
            }
        }
    }

}
