package co.simplon.alt6.oop;

public class Bird extends Animal {

    @Override
    public void breath() {
        System.out.println("Bird is breathing air...");
    }
    
}
