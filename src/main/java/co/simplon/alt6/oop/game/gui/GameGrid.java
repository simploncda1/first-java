package co.simplon.alt6.oop.game.gui;

import co.simplon.alt6.oop.game.TicTacToe;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class GameGrid extends GridPane  {
    private TicTacToe game;

    public GameGrid() {

        this.game = new TicTacToe();
    }

    public void draw() {

        char[][] grid = this.game.getGrid();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Button btn = new Button("" + grid[i][j]);
                final int row = i;
                final int col = j;
                btn.setOnAction(e -> {
                    game.place(row, col);
                    draw();
                });
                add(btn, i, j);
            }
        }

    }

}
