package co.simplon.alt6.oop;

import co.simplon.alt6.oop.interfaces.HTMLConvertable;

public class Task implements HTMLConvertable{
    private String label;
    protected boolean done = false;

    public Task(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        if (!this.done) {
            return "☐ "+this.label;
        }
        return "☑ "+this.label;
    }

    public void toggle() {
        this.done = !this.done;
    }

    @Override
    public String toHTML() {
        
        return "<input type=\"checkbox\"><label>"+label+"</label>";
    }

}
