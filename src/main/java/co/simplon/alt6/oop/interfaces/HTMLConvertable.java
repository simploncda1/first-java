package co.simplon.alt6.oop.interfaces;

public interface HTMLConvertable {
    String toHTML();
}
