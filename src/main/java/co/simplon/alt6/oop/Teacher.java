package co.simplon.alt6.oop;

import java.util.ArrayList;
import java.util.List;
/**
 * La classe Teacher hérite de la classe Person, elle possède donc toutes ses 
 * méthodes et propriétés automatiquement et peut définir ses propres méthodes et
 * propriétés (ici la liste de skills)
 * 
 */
public class Teacher extends Person {
    private List<String> skills = new ArrayList<>();

    public Teacher(String name, String firstName, int age) {
        super(name, firstName, age);
    }

    /**
     * On redéfinit ici la méthode getFullName du parent Person, ce qui fait que
     * si Teacher est utilisé en tant que Person quelque part dans le code et qu'on
     * utilise le getFullName, c'est celui ci qui sera appelé
     */
    @Override
    public String getFullName() {
        return "Teacher "+super.getFullName();
    }

    
}
