package co.simplon.alt6.oop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Promo {
    private List<Person> students = new ArrayList<>();
    private int capacity;

    Promo(List<Person> students, int capacity) {
        this.students = students;
        this.capacity = capacity;
    }

    public Promo(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Méthode permettant de savoir si une personne fait déjà partie de la promo
     * 
     * @param fullName Le nom complet de la personne cherchée
     * @return true si dans la promo, false sinon
     */
    public boolean isStudent(String fullName) {
        for (Person person : this.students) { // for(let person of students) foreach($students as $person)
            if (person.getFullName().equalsIgnoreCase(fullName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Ajoute une personne à la promo si elle n'en fait pas déjà partie et que la
     * capacité max n'est pas déjà atteinte
     * 
     * @param person La personne à ajouter à la promo
     */
    public void addStudent(Person person) {
        if (person instanceof Teacher) {
            if(hasTeacher()){
                return;
            }
            this.students.add(person);

        } else {

            if (students.size() < this.capacity && !this.isStudent(person.getFullName())) {
                this.students.add(person);
            }
        }
    }

    private boolean hasTeacher() {
        for (Person person : students) {
            if (person instanceof Teacher) {
                return true;
            }
        }
        return false;
    }

    public void seatPlan(int tableCount) {

        List<Person> randomized = new ArrayList<>(this.students);
        Collections.shuffle(randomized);
        Iterator<Person> persIterator = randomized.iterator();

        String[] orderedDisplay = new String[tableCount];
        Arrays.fill(orderedDisplay, 0, tableCount, "");

        while (persIterator.hasNext()) {

            for (int i = 1; i <= tableCount; i++) {
                if (persIterator.hasNext()) {
                    Person person = persIterator.next();
                    orderedDisplay[i - 1] += person.getFullName() + "\n";

                }
            }

        }
        for (int i = 0; i < orderedDisplay.length; i++) {
            System.out.println("Table " + (i + 1) + "\n" + orderedDisplay[i]);
        }

    }

}
