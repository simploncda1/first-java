package co.simplon.alt6.oop;

import java.util.ArrayList;
import java.util.List;

import co.simplon.alt6.oop.interfaces.HTMLConvertable;

public class HTMLDocument implements HTMLConvertable {
    private List<HTMLConvertable> liste = new ArrayList<>();

    public void addElement(HTMLConvertable element) {
        liste.add(element);
    }

    @Override
    public String toHTML() {
        String html = "<html><head></head><body>";

        for (HTMLConvertable item : liste) {
            html+=item.toHTML();
        }
        return html+"</body></html>";

    }


}
