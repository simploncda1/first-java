package co.simplon.alt6.oop;

import java.util.ArrayList;
import java.util.List;

public class TodoList {
    List<String> tasks;

    public TodoList(List<String> initial) {
        this.tasks = initial;
    }

    public TodoList() {
        this.tasks = new ArrayList<>();
    }

    public String draw() {
        String displayText = "";
        for (String task : tasks) {
            displayText += "☐ "+task+"\n";
        }
        return displayText;
    }

    public void add(String newTask) {
        tasks.add(newTask);
    }
    
}
