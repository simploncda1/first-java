package co.simplon.alt6.dao.interfaces;


import co.simplon.alt6.entity.Dog;

public interface DogDao extends Dao<Dog> {
}
