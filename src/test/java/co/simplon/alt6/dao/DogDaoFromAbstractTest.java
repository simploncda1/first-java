package co.simplon.alt6.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.alt6.entity.Dog;

public class DogDaoFromAbstractTest {
    DogDaoFromAbstract dao;
    @BeforeEach
    void setUp() {
        dao = new DogDaoFromAbstract();
        try {
            ScriptRunner runner = new ScriptRunner(Connector.getConnection());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAdd() {
        Dog toPersist = new Dog("test", "test", 10);
        assertTrue(dao.add(toPersist));
        assertNotNull(toPersist.getId());
        assertEquals(4, toPersist.getId());
    }

    @Test
    void testDelete() {
        assertTrue(dao.delete(1));
    }

    @Test
    void testDeleteNoMatchingId() {
        assertFalse(dao.delete(100));
    }

    @Test
    void testGetAll() {
        List<Dog> result = dao.getAll();

        assertEquals(3,result.size());

        assertNotNull(result.get(0).getName()); //Moins précis mais un poil plus maintenable

        //Se casse plus facilement mais bien précis
        assertEquals(1, result.get(0).getId());
        assertEquals("Fido", result.get(0).getName());
        assertEquals("Corgi", result.get(0).getBreed());
        assertEquals(3, result.get(0).getAge());
    }

    @Test
    void testGetById() {
        Dog result = dao.getById(1);

        assertNotNull(result);

        assertEquals(1, result.getId());
        assertEquals("Fido", result.getName());
        assertEquals("Corgi", result.getBreed());
        assertEquals(3, result.getAge());

    }
    @Test
    void testGetByIdNoResult() {
        Dog result = dao.getById(100);

        assertNull(result);

    }

    @Test
    void testUpdate() {
        Dog toUpdate = new Dog(1, "test", "test", 10);
        assertTrue(dao.update(toUpdate));
    }

    @Test
    void testUpdateNoMatchingId() {
        Dog toUpdate = new Dog(100, "test", "test", 10);
        assertFalse(dao.update(toUpdate));
    }
}
