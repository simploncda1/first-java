package co.simplon.alt6.oop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

public class PromoTest {
    List<Person> sample = List.of(
        new Person("Test", "toust", 25), 
        new Person("Tost", "tust", 55)
    );

    @Test
    void testIsStudentNotPresent() {
        Promo promo = new Promo(sample, 5);
        boolean result = promo.isStudent("Test test");
        assertFalse(result);
    }
    @Test
    void testIsStudentPresent() {
        Promo promo = new Promo(sample, 5);
        boolean result = promo.isStudent("Toust Test");
        assertTrue(result);
    }
}
