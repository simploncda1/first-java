package co.simplon.alt6.oop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class TaskTest {

    @Test
    void testShouldInstanciate() {
        Task task = new Task("Do test");
        assertNotNull(task);
    }


    @Test
    void testShouldConvertToString() {
        Task task = new Task("Do test");
        
        assertEquals("☐ Do test", task.toString());

    }


    @Test
    void testShouldToggleDone() {
        Task task = new Task("Do test");
        task.toggle();
        assertEquals("☑ Do test", task.toString());

    }
    @Test
    void testShouldToggleTwice() {
        Task task = new Task("Do test");
        task.toggle();
        task.toggle();
        assertEquals("☐ Do test", task.toString());

    }
}
