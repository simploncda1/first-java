package co.simplon.alt6.oop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

public class ComplexTaskTest {

    @Test
    void testShouldConvertItselfAndSubtasksToString() {
        ComplexTask complexTask = new ComplexTask("Do test",
                List.of(
                        new Task("sub1"),
                        new Task("sub2")));

        assertEquals("☐ Do test\n  ☐ sub1\n  ☐ sub2", complexTask.toString());
    }

    @Test
    void testShouldConvertItselfAndSubtasksWithDone() {
        Task taskDone = new Task("sub1");
        taskDone.toggle();
        ComplexTask complexTask = new ComplexTask("Do test",
                List.of(
                        taskDone,
                        new Task("sub2")));

        assertEquals("☐ Do test\n  ☑ sub1\n  ☐ sub2", complexTask.toString());
    }

    @Test
    void testShouldToggleItselfAndSubTasks() {
        ComplexTask complexTask = new ComplexTask("Do test",
                List.of(
                        new Task("sub1"),
                        new Task("sub2")));
        complexTask.toggle();

        assertEquals("☑ Do test\n  ☑ sub1\n  ☑ sub2", complexTask.toString());
    }

    @Test
    void testShouldPutAllToDone() {
        Task taskDone = new Task("sub1");
        taskDone.toggle();
        ComplexTask complexTask = new ComplexTask("Do test",
                List.of(
                        taskDone,
                        new Task("sub2")));
        complexTask.toggle();
        assertEquals("☑ Do test\n  ☑ sub1\n  ☑ sub2", complexTask.toString());
    }

    @Test
    void testShouldBeRecursive() {
        ComplexTask complexTask = new ComplexTask("Do test",
                List.of(
                        new Task("sub1"),
                        new Task("sub2"),
                        new ComplexTask("Do test",
                                List.of(
                                        new Task("sub1"),
                                        new Task("sub2")))));
        complexTask.toggle();

        assertEquals("☑ Do test\n  ☑ sub1\n  ☑ sub2\n  ☑ Do test\n  ☑ sub1\n  ☑ sub2", complexTask.toString());
    }
}
