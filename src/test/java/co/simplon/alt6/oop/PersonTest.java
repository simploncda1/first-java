package co.simplon.alt6.oop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PersonTest {
    Person person;
    PrintStream standardOut = System.out;
    ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        person = new Person("Name", "Firstname", 45);
        System.setOut(new PrintStream(outputStreamCaptor));
    }
    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    void testGetFullName() {
        String result = person.getFullName();

        assertEquals("Firstname Name", result);
    }

    @Test
    void testSetAgeSuccessful() {
        person.setAge(20);
        assertEquals(20, person.getAge());
    }

    @Test
    void testSetAgeShouldThrowWithNegative() {
        assertThrows(IllegalArgumentException.class, () -> person.setAge(-20));

    }

    @Test
    void testGreetingNoException() {
        person.greeting();
        assertEquals("Hello, my name is Firstname Name I am 45 years old", outputStreamCaptor.toString().trim());
    }

    @Test
    void testSaluteSamePerson() {
        person.salute(person);
        assertEquals("forever alone", outputStreamCaptor.toString().trim());
    }

    @Test
    void testSaluteSameFamily() {
        person.salute(new Person("Name", "Bloup", 20));
        assertEquals("Salut la famille", outputStreamCaptor.toString().trim());
    }

    @Test
    void testSaluteSameBelowForty() {
        person.setAge(34);
        person.salute(new Person("Blip", "Bloup", 20));
        assertEquals("check", outputStreamCaptor.toString().trim());
    }

    @Test
    void testSaluteStandard() {
        person.salute(new Person("Blip", "Bloup", 20));
        assertEquals("salut formel", outputStreamCaptor.toString().trim());
    }
}
