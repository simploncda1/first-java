package co.simplon.alt6.oop.game;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class TicTacToeTest {

    @Test
    void testShouldInstanciate() {
        TicTacToe game = new TicTacToe();
        assertNotNull(game);
    }

    @Test
    void testShouldHaveEmptyGrid() {
        TicTacToe game = new TicTacToe();
        char[][] expected = {
            {' ', ' ',' '},
            {' ', ' ',' '},
            {' ', ' ',' '}
        };
        assertArrayEquals(expected, game.getGrid());
    }

    @Test
    void testShouldAcceptInitialState() {
        char[][] expected = {
            {'X', ' ',' '},
            {' ', 'O',' '},
            {' ', ' ',' '}
        };
        TicTacToe game = new TicTacToe(expected);
        assertArrayEquals(expected, game.getGrid());
    }


    @Test
    void testShouldPlaceSymbol() {
        char[][] expected = {
            {' ', ' ',' '},
            {' ', 'X',' '},
            {' ', ' ',' '}
        };
        TicTacToe game = new TicTacToe();

        game.place(1,1);

        assertArrayEquals(expected, game.getGrid());
    }

    @Test
    void testShouldAltenateSymbols() {
        char[][] expected = {
            {'O', ' ',' '},
            {' ', 'X',' '},
            {' ', ' ',' '}
        };
        TicTacToe game = new TicTacToe();

        game.place(1,1);        
        game.place(0,0);

        assertArrayEquals(expected, game.getGrid());
    }

    @Test
    void testShouldNotPlaceOnNotEmpty() {
        char[][] expected = {
            {' ', ' ',' '},
            {' ', 'X',' '},
            {' ', ' ',' '}
        };
        TicTacToe game = new TicTacToe();

        game.place(1,1);        
        game.place(1,1);

        assertArrayEquals(expected, game.getGrid());
    }

    @Test
    void testShouldNotPlaceOutOfBound() {
        char[][] expected = {
            {' ', ' ',' '},
            {' ', ' ',' '},
            {' ', ' ',' '}
        };
        TicTacToe game = new TicTacToe();

        game.place(4,3);

        assertArrayEquals(expected, game.getGrid());
    }

    @Test
    void testShouldWinWithRow() {
        char[][] winningGrid = {
            {' ', ' ',' '},
            {' ', ' ',' '},
            {'X', 'X',' '},
        };
        TicTacToe game = new TicTacToe(winningGrid);
        game.place(2, 2);
        assertEquals(TicTacToe.X_WIN, game.winState());

    }

    @Test
    void testOShouldWinToo() {
        char[][] winningGrid = {
            {' ', ' ',' '},
            {' ', ' ',' '},
            {'O', 'O',' '},
        };
        TicTacToe game = new TicTacToe(winningGrid);
        game.place(1, 2);
        game.place(2, 2);
        assertEquals(TicTacToe.O_WIN, game.winState());
    }

    @Test
    void testShouldWinWithCol() {
        char[][] winningGrid = {
            {' ', ' ',' '},
            {' ', 'X',' '},
            {' ', 'X',' '},
        };
        TicTacToe game = new TicTacToe(winningGrid);
        game.place(0, 1);
        assertEquals(TicTacToe.X_WIN, game.winState());

    }
    @Test
    void testShouldWinWithDiag1() {
        char[][] winningGrid = {
            {' ', ' ',' '},
            {' ', 'X',' '},
            {'X', ' ',' '},
        };
        TicTacToe game = new TicTacToe(winningGrid);
        game.place(0, 2);
        assertEquals(TicTacToe.X_WIN, game.winState());

    }

    @Test
    void testShouldWinWithDiag2() {
        char[][] winningGrid = {
            {'X', ' ',' '},
            {' ', 'X',' '},
            {' ', ' ',' '},
        };
        TicTacToe game = new TicTacToe(winningGrid);
        game.place(2, 2);
        assertEquals(TicTacToe.X_WIN, game.winState());

    }

    @Test
    void testShouldDrawWhenFull() {
        char[][] winningGrid = {
            {'X', 'O','X'},
            {'O', 'X','X'},
            {'O', ' ','O'},
        };
        TicTacToe game = new TicTacToe(winningGrid);
        game.place(2, 1);
        assertEquals(TicTacToe.DRAW, game.winState());

    }

}
